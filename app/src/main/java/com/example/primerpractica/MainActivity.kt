package com.example.primerpractica

import androidx.appcompat.app.AppCompatActivity
import android.os.Bundle
import android.widget.Toast
import kotlinx.android.synthetic.main.activity_main.*

class MainActivity : AppCompatActivity() {
    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_main)
        saludo()
        println(saludo2("Alexis"))
    btnSaludar.setOnClickListener {
        Toast.makeText(this, "Hi ${editNombre.text}", Toast.LENGTH_LONG).show()
    }
    }

    private fun saludo() {
        val nombre = "Alexis"
        val apellido = "Lopez"
        var edad = 21

        //println("Hola " + nombre + " " + apellido)
        print("Hola $nombre $apellido, tu edad es de_ ${2020-1998} años")
        println("Tu edad es: " + edad)
    }
    fun saludo2(nombre: String): String {
    return "Hola 2: $nombre"
    }
}